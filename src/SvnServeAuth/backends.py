import logging
import re
import sre_constants
import sys

from django.conf import settings
from django.contrib.auth.models import User
from djblets.util.misc import get_object_or_none
from trac.config import Configuration


class SvnServeBackend(object):

	def authenticate(self, username, password):
		conf = Configuration('/home/svn/conf/passwd')
		if username in conf['users']:
			if password == conf.get('users', username):
				return self.get_or_create_user(username)

	def get_or_create_user(self, username):
		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			try:
				user = User(username=username,
					password='',
					first_name='',
					last_name='',
					email='')
				user.is_staff = False
				user.is_superuser = False
				user.set_unusable_password()
				user.save()
			except :
				pass
		return user

	def get_user(self, user_id):
		return get_object_or_none(User, pk=user_id)
